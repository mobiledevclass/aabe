package com.aabe.aabeapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void btnMembershipLink (View view)
	{
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
				Uri.parse("http://aabe.org/docs/pages/645/file/2014%20Member%20Dues%20Invoice%20revised.doc"));
		startActivity(browserIntent);
    }
}

